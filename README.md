
This is a collection of Feature Vector JSON files that are the result
of all the feature extraction and processing.  There are symlinks to
the original APK that was processed, and a .json file that is the
feature vector for that APK.

The feature vector JSON data format is documented here:
https://gitlab.com/trackingthetrackers/wiki/-/wikis/JSON-format-definition-for-feature-vectors

They were generated from:
* https://gitlab.com/trackingthetrackers/scripts
* https://gitlab.com/trackingthetrackers/extracted-features


The _feature_vectors.json_ file at the root here is the collection of
all values seen in the current data collection.  This is used to
specify the column list for "bitmask" entries of which features are
present in a given APK.


## APK Data Set

We worked with a vast collection of APKs, including many proprietary
APKs where we do not have a clear license to reshare them.  Therefore,
we cannot make our APK collection available for download.  Instead, we
have made a listing of all the APKs we used that are available in
[androzoo](https://androzoo.uni.lu/lists).  Those with access to that
collection can then download the APKs from there to recreate this
work.

This listing is available as [index-androzoo-latest.csv](index-androzoo-latest.csv).
