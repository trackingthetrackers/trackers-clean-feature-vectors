#!/usr/bin/env python3
#
# generate an index of an APK collection following the pattern in
# https://androzoo.uni.lu/lists
#
# run in the background at low priority with:
#   nice -n 19 ionice -c3 ~/code/trackingthetrackers/scripts/index-apks.py

import glob
import os
import sys
import yaml

data = dict()
with open(sys.argv[1], 'rb') as fp:
    for line in fp:
        data[line[:64]] = line

in_apk_set = [b'0000003B455A6C7AF837EF90F2EAFFD856E3B5CF49F5E27191430328DE2FA670']
sha256_to_f = dict()
for f in glob.glob('clean/*/*/*.apk') + glob.glob('trackers/*/*/*.apk'):
    sha256 = os.path.basename(f[:-4]).upper()
    in_apk_set.append(sha256.encode())
    sha256_to_f[sha256] = f

found = []
found_set = set()
print('in_apk_set:', len(in_apk_set))
in_androzoo = 0
with open('index-androzoo-latest.csv', 'wb') as fp:
    fp.write(b'sha256,sha1,md5,dex_date,apk_size,pkg_name,vercode,vt_detection,vt_scan_date,dex_size,markets\n')
    for f in sorted(in_apk_set):
        line = data.get(f)
        if line:
            in_androzoo += 1
            fp.write(line)
            found.append(f)
            found_set.add(f)
            print('.', end='', flush=True)
print('\nin_apk_set:', len(in_apk_set), '\tin androzoo:', in_androzoo)
print('found:', len(found), len(found_set))

not_in_androzoo = []
for x in in_apk_set:
    if x not in found_set:
        f = sha256_to_f.get(x)
        if f:
            not_in_androzoo.append(f)
with open('index-not-in-androzoo-latest.csv.yaml', 'w') as fp:
    yaml.dump(sorted(not_in_androzoo))
